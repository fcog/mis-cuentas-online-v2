<?php

return [
    'TYPE' => [
        'expense' => 'Gasto',
        'income' => 'Ingreso',
        'saving' => 'Ahorro'
    ],
    'TYPE_INDEX' => [
        'expense' => 1,
        'income' => 2,
        'saving' => 3
    ],
    'PAYMENT_TYPE' => [
        'debit' => 'Débito',
        'credit' => 'Crédito',        
    ],
    'PAYMENT_TYPE_INDEX' => [
        'debit' => 1,
        'credit' => 2,
    ],
];