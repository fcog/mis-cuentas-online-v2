<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccountsStatisticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('account_statistics')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'month' => Carbon::now()->format('Y-m-d H:i:s'),
            'account_id' => 1,
            'user_id' => 1,
            'total_amount' => 0,
            'total_transactions' => 0
        ]);
        DB::table('account_statistics')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'month' => Carbon::now()->format('Y-m-d H:i:s'),
            'account_id' => 2,
            'user_id' => 1,
            'total_amount' => 0,
            'total_transactions' => 0
        ]);  
        DB::table('account_statistics')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'month' => Carbon::now()->format('Y-m-d H:i:s'),
            'account_id' => 3,
            'user_id' => 1,
            'total_amount' => 0,
            'total_transactions' => 0
        ]);  
	    DB::table('account_statistics')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'month' => Carbon::now()->format('Y-m-d H:i:s'),
            'account_id' => 4,
            'user_id' => 1,
            'total_amount' => 0,
            'total_transactions' => 0
        ]);  
        DB::table('account_statistics')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'month' => Carbon::now()->format('Y-m-d H:i:s'),
            'account_id' => 5,
            'user_id' => 1,
            'total_amount' => 0,
            'total_transactions' => 0
        ]);  
        DB::table('account_statistics')->insert([
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),        	
            'month' => Carbon::now()->format('Y-m-d H:i:s'),
            'account_id' => 6,
            'user_id' => 1,
            'total_amount' => 0,
            'total_transactions' => 0
        ]);          
    }
}
