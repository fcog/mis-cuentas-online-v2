<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('account_statistics');

        Schema::table('transactions', function($table)
        {        
            $table->dropColumn(array('account_id'));         
        });       

        Schema::drop('accounts');         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('account_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('month');
            $table->integer('user_id')->unsigned();
            $table->integer('account_id');
            $table->integer('total_amount');
            $table->integer('total_transactions')->default(0);

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->foreign('account_id')
                  ->references('id')
                  ->on('accounts')
                  ->onDelete('cascade');
        });

        Schema::table('transactions', function($table)
        {
            $table->integer('account_id')->after('type_id')->nullable()->unsigned();
            $table->integer('account2_id')->after('type_id')->nullable()->unsigned();
        });

        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            //type: 1.debit 2.credit 3.cash 4.debt
            $table->integer('type_id')->unsigned();
            $table->string('title');
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });                
    }
}
