<div class="form-group">

    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class'=>'form-control']) !!}

</div>

<div class="form-group">

    {!! Form::label('calendar_initial_day', 'Dia inicial de calendario:') !!}
    {!! Form::input('number', 'calendar_initial_day', null, ['class'=>'form-control', 'maxlength'=>2, 'min'=>1, 'max'=>31]) !!}

</div>

<div class="form-group">

    {!! Form::submit($submitButtonText, ['class'=>'btn btn-success']) !!}

    <a class="btn btn-link pull-right" href="{{ route('users.changePassword') }}">Cambiar clave</a>

</div>

@section('javascript')

@stop