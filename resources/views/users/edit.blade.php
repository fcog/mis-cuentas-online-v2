@extends('layout')

@section('content')

    <div class="page-header">
        <h1>Editar perfil de usuario y opciones de la aplicación</h1>
    </div>

    <div class="alert alert-warning" role="alert">
    	<p><label>Usuario desde:</label> {{ $user->created_at }}</p>
    </div>

    @include('errors._form_errors')

    <div class="well">

        {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UsersController@update', $user]]) !!}

            @include('users._form', ['submitButtonText' => 'Guardar'])

        {!! Form::close() !!}

    </div>

@stop
