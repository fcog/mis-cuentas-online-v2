@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Crear una transacción fija</h1>
	</div>

	@include('errors._form_errors')

	<div class="well">

		{!! Form::open(['url' => 'fixed-transactions']) !!}

			@include('fixed_transactions._form', ['submitButtonText' => 'Crear'])

		{!! Form::close() !!}

	</div>

@stop

@section('javascript')
	<script>
		$('#type_id').change(function()
		{
			$.get('/api/categories/type/' + this.value, function(categories)
			{
				var $category = $('#category_id');

				$category.find('option').remove().end();

				$.each(categories, function(index, category) {
					$category.append('<option value="' + index + '">' + category + '</option>');
				});
			});
		});
	</script>
@stop