@extends('layout')

@section('content')

	<h1>Gasto Fijo #{{ $fixed_expense->id }}</h1>

	<p>Categoria: {{ $fixed_expense->category->title }}</p>
	<p>Nombre: {{ $fixed_expense->title }}</p>
	<p>Monto aproximado a pagar: {{ $fixed_expense->amount }}</p>
	<p>Dia Límite de Pago: {{ $fixed_expense->day }}</p>

@stop

@section('footer')

@stop