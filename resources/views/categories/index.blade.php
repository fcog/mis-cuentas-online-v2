@extends('layout')

@section('content')

	<div class="page-header">
		<h1>Categorias</h1>
	</div>

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">Acciones</div>
		<div class="panel-body">
			<a class="btn btn-success" href="{{ route('categories.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear nueva</a>
			<a class="btn btn-default" href="{{ route('categories.indexInactive') }}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Ver inactivas</a>
		</div>
	</div>

	@if (count($categories))
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
			        <tr>
			          <th>Nombre</th>
			          <th>Categoria</th>
			          <th>Acción</th>
			        </tr>
		        </thead>
		        <tbody>
				@foreach ($categories as $category)

					<tr>
						<td>{{ $category->title }}</td>
						<td>{{ $category->getType() }}</td>
						<td>
							<a alt="Editar" title="Editar" href="{{ route('categories.edit', $category) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
							<a alt="Desactivar" title="Desactivar" href="{{ route('categories.deactivate', $category) }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						</td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="alert alert-warning" role="alert">No hay datos</div>
	@endif

@stop

@section('footer')

@stop