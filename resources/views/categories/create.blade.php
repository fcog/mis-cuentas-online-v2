@extends('layout')


@section('content')

	<div class="page-header">
		<h1>Crear una categoria</h1>
	</div>

	@include('errors._form_errors')

	<div class="well">

		{!! Form::open(['url' => 'categories']) !!}

			@include('categories._form', ['submitButtonText' => 'Crear'])

		{!! Form::close() !!}

	</div>

@stop
