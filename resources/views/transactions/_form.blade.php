<div class="form-group" ng-class="{ 'has-error': ( transactionForm.category_id.$touched || transactionForm.$submitted ) && transactionForm.category_id.$invalid }">

    {!! Form::label('category_id', 'Categoria:') !!}

    <select class="form-control" name="category_id" ng-model="transactionFormData.category_id" required ng-options="c.id as c.title for c in categories" ng-show="!showNewCategoryForm" ng-change="checkAddNewCategory()">
        <option value="">--Seleccione--</option>
    </select>

    <div class="form-group" ng-show="showNewCategoryForm">
        <input class="form-control" type="text" ng-model="value" /><br>
        <a class="btn btn-success" ng-click="addCategoryToDropDown(value)"><span class="glyphicon glyphicon-floppy-save"></span> Agregar categoría nueva</a>
        <a class="btn btn-warning" ng-click="cancelNewCategory()"><span class="glyphicon glyphicon-remove"></span> Cancelar</a>        
    </div>

    <div class="help-block" ng-messages="transactionForm.category_id.$error" ng-if="transactionForm.category_id.$touched || transactionForm.$submitted">
        <p ng-message="required">La categoría es obligatoria.</p>
    </div>

</div>

<div class="form-group" ng-class="{ 'has-error': ( transactionForm.title.$touched || transactionForm.$submitted ) && transactionForm.title.$invalid }">

    {!! Form::label('title', 'Título:') !!}
    {!! Form::text('title', null, ['class'=>'form-control', 'ng-model' => 'transactionFormData.title', 'ng-minlength' => '3', 'ng-maxlength' => '80', 'required' => 'required']) !!}

    <div class="help-block" ng-messages="transactionForm.title.$error" ng-if="transactionForm.title.$touched || transactionForm.$submitted">
        <p ng-message="minlength">El título está muy corto.</p>
        <p ng-message="maxlength">El título está muy largo.</p>
        <p ng-message="required">El título es obligatorio.</p>
    </div>

</div>

<div class="form-group" ng-class="{ 'has-error': ( transactionForm.amount_paid.$touched || transactionForm.$submitted ) && transactionForm.amount_paid.$invalid }">

    {!! Form::label('amount_paid', 'Monto:') !!}
    <div class="input-group">
        <span class="input-group-addon">$</span>
        {!! Form::input('number','amount_paid', null, ['class'=>'form-control', 'ng-model' => 'transactionFormData.amount_paid', 'ng-minlength' => '2', 'ng-maxlength' => '10', 'required' => 'required']) !!}
    </div>

    <div class="help-block" ng-messages="transactionForm.amount_paid.$error" ng-if="transactionForm.amount_paid.$touched || transactionForm.$submitted">
        <p ng-message="minlength">El monto es muy poco.</p>
        <p ng-message="maxlength">El monto es demasiado.</p>
        <p ng-message="required">El monto es obligatorio.</p>
    </div>

</div>

<div class="form-group" ng-class="{ 'has-error': ( transactionForm.payment_type_id.$touched || transactionForm.$submitted ) && transactionForm.payment_type_id.$invalid }" ng-show="isExpense()">

    <div>
        {!! Form::label('payment_type_id', 'Tipo de pago:') !!}

        <input type="radio" name="payment_type_id" ng-model="transactionFormData.payment_type_id" id="debit" value="1"> <label for="debit">Débito</label>
        <input type="radio" name="payment_type_id" ng-model="transactionFormData.payment_type_id" id="credit" value="2"> <label for="credit">Crédito</label>
    </div>
    
    <div class="help-block" ng-messages="transactionForm.payment_type_id.$error" ng-if="transactionForm.payment_type_id.$touched || transactionForm.$submitted">
    </div>

</div>

<div class="form-group" ng-class="{ 'has-error': ( transactionForm.paid_date_temp.$touched || transactionForm.$submitted ) && transactionForm.paid_date_temp.$invalid }">

    {!! Form::label('paid_date', 'Fecha de pago:') !!}

    <input type="text" name="paid_date" class="form-control" ng-model="transactionFormData.paid_date_temp" uib-datepicker-popup="[[format]]" is-open="open1" data-ng-click="open1 = true">

    <div class="help-block" ng-messages="transactionForm.paid_date_temp.$error" ng-if="transactionForm.paid_date_temp.$touched || transactionForm.$submitted">
    </div>

</div>

<div class="form-group" ng-class="{ 'has-error': transactionForm.payment_date_limit_temp.$touched && transactionForm.payment_date_limit_temp.$invalid }">

    {!! Form::label('payment_date_limit', 'Fecha límite de pago:') !!}

    <input type="text" name="payment_date_limit" class="form-control" ng-model="transactionFormData.payment_date_limit_temp" uib-datepicker-popup="[[format]]" is-open="open2" data-ng-click="open2 = true">

    <div class="help-block" ng-messages="transactionForm.payment_date_limit_temp.$error" ng-if="transactionForm.payment_date_limit_temp.$touched || transactionForm.$submitted">

    </div>

</div>

<div class="form-group">

    <button class="btn btn-success" type="submit" ng-disabled="disable"><span class="glyphicon glyphicon-floppy-save"></span> Guardar</button>
    <button class="btn btn-warning" type="button" ng-click="cancel()"><span class="glyphicon glyphicon-remove"></span> Cancelar</button>

    <button ng-show="transactionFormData.id" class="btn btn-danger pull-right" type="button" confirmed-click="delete( transactionFormData )" ng-confirm-click="¿Realmente quiere borrar permanentemente esta transacción?" ng-disabled="disable"><span class="glyphicon glyphicon-trash"></span> Borrar</button>

</div>