@extends('layout')

@section('content')

	<div class="page-header">
		<h1>Gastos</h1>
	</div>

	<div class="panel panel-primary">
		<!-- Default panel contents -->
		<div class="panel-heading">Acciones</div>
		<div class="panel-body">
			<a class="btn btn-success" href="{{ route('expenses.create') }}" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear nuevo</a>
		</div>
	</div>

	@if (count($transactions))
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
			        <tr>
						<th>Nombre</th>
						<th>Categoria</th>
						<th>Monto</th>
						<th>Pagado con</th>
						<th>Fecha de Pago</th>
						<th>Fecha límite de pago</th>
						<th>Acción</th>
			        </tr>
		        </thead>
		        <tbody>
				@foreach ($transactions as $transaction)

					<tr>
						<td>{{ $transaction->title }}</td>
						<td>{{ $transaction->category->title }}</td>
						<td>{{ $transaction->present()->amount_paid_formatted }}</td>
						<td>{{ $transaction->getAccount() }}</td>
						<td>{{ $transaction->present()->paid_date_formatted }}</td>
						<td>{{ $transaction->present()->payment_date_limit_formatted }}</td>
						<td>
							<a alt="Editar" title="Editar" href="{{ route('expenses.edit', $transaction) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

							{!! Form::model($transaction, ['method' => 'DELETE', 'action' => ['ExpensesController@destroy', $transaction], 'id' => 'delete' . $transaction->id, 'class' => 'delete']) !!}
								<a href="javascript:;" onclick="document.getElementById('delete{{ $transaction->id }}').submit();" title="Borrar permanentemente"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
							{!! Form::close() !!}
						</td>
					</tr>

				@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="alert alert-warning" role="alert">No hay datos</div>
	@endif

@stop

@section('footer')

@stop