<div class="modal-header">
	<h1 class="modal-title">[[ title ]]</h1>
</div>

<div class="modal-body">

	@include('errors._form_errors_js')

	<form name="transactionForm" ng-submit="transactionForm.$valid && submit()" novalidate>

		@include('transactions._form')

	</form>

</div>

