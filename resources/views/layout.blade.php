<!DOCTYPE html>
<html>
<head>
	<title>Mis Cuentas Online</title>

    <meta name=viewport content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/all.css') }}" >

    <link rel="stylesheet" href="/css/angular-chart.css">

	@yield('header')

</head>
<body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Mis Cuentas Online</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          @if($user)
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ route('fixed-transactions.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                        Transacciones Fijas
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('fixed-transactions.index') }}">Ver Transacciónes Fijas</a></li>
                        <li><a href="{{ route('fixed-transactions.indexInactive') }}">Ver Transacciónes Fijas Inactivas</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('fixed-transactions.create') }}">Crear Transacción Fija</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('categories.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                        Categorias
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('categories.index') }}">Ver Categorias</a></li>
                        <li><a href="{{ route('categories.indexInactive') }}">Ver Categorias Inactivas</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('categories.create') }}">Crear Categoria</a></li>
                    </ul>
                </li>          
            </ul>
          @endif
          <ul class="nav navbar-nav navbar-right">
              <li class="active">
                  @if($user)
                      <a href="{{ route('users.index') }}" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true">
                          <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                          {{ $user->name }}
                          <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu">
                          <li><a href="{{ route('users.index') }}">Perfil</a></li>
                          <li><a href="{{ route('users.index') }}">Opciones</a></li>
                          <li><a href="#">Sobre la aplicación</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="/auth/logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Salir</a></li>
                      </ul>
                  @else
                     <a href="/auth/login"><span class="glyphicon glyphicon-user" aria-hidden="true"></span><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Ingresar</a>
                  @endif
              </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

  @yield('container', '<div class="container">')
    @if (Session::has('flash_message'))
        <div class="alert alert-{{ Session::get('flash_action') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_message') }}
        </div>
    @endif

		@yield('content')

	</div>

	@yield('footer')

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>

    <script type="text/javascript">
      $(function() {
        //enable nav dropdown menu on hover
        $('.nav li').hover(function(){
            $(this).addClass('open');
        }, function(){
            $(this).removeClass('open');
        });
      });

    </script>

    @yield('javascript')

</body>
</html>