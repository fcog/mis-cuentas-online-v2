@extends('layout')

@section('container')
    <div class="container-fluid">
@stop

@section('content')

    @if ($user)

        <div id="main-content" class="row" ng-app="miscuentas" ng-controller="HomeController">

            <div class="col-lg-7">

                @if (!count($categories))
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-info-sign"></span> Aún no tienes categorias. Crea una <a href="{{ route('categories.create') }}">Aquí</a>.
                    </div>
                @endif

                <div id="data-container">

                    <div class="panel panel-primary">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            Acciones
                            <div class="pull-right" id="loading" ng-show="loadingText">
                                <span ng-cloak>[[ loadingText ]]</span><img src="/images/gear.svg">
                            </div>
                        </div>

                        <div class="panel-body">

                            <div id="main-actions" class="col-sm-2">

                                <a class="btn btn-danger" href="javascript:;" ng-click="open( 'store', {{ config('constants.TYPE_INDEX.expense') }} )" role="button"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                <a class="btn btn-success" href="javascript:;" ng-click="open( 'store', {{ config('constants.TYPE_INDEX.income') }} )" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </a>

                            </div>

                            @if (!count($transactions))

                                <div class="alert alert-warning alert-dismissible col-sm-10" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                    Presiona el botón verde para agregar un ingreso y el botón rojo para agregar un gasto
                                </div>

                            @else

                                <div id="date-picker-start" class="col-sm-3">

                                    <input type="hidden" uib-datepicker-popup="[[format]]" is-open="openDateStart" close-on-date-selection="true" show-button-bar="false" ng-model="dateStart" class="datepicker well well-sm" ng-change="onDateChange()" >
                                    Desde <span ng-bind="dateStart | date:'MMMM dd yyyy'"></span>  
                                    <button type="button" class="btn btn-default" ng-click="openDatePickerStart()"><i class="glyphicon glyphicon-calendar"></i></button>

                                </div>

                                <div id="date-picker-end" class="col-sm-3">

                                    <input type="hidden" uib-datepicker-popup="[[format]]" is-open="openDateEnd" close-on-date-selection="true" show-button-bar="false" ng-model="dateEnd" class="datepicker well well-sm" ng-change="onDateChange()" >
                                    Hasta <span ng-bind="dateEnd | date:'MMMM dd yyyy'"></span>  
                                    <button type="button" class="btn btn-default" ng-click="openDatePickerEnd()"><i class="glyphicon glyphicon-calendar"></i></button>

                                </div>

                                <div id="filter" class="col-sm-4">

                                    <div class="input-group">
                                        {!! Form::input('text','search', null, ['class'=>'form-control', 'placeholder' => 'Buscar', 'ng-model' => 'filterData', 'ng-change' => 'onFilterChange()']) !!}
                                        <div class="input-group-btn">
                                            <span class="btn btn-default"><i class="glyphicon glyphicon-search"></i></span>
                                        </div>
                                    </div>

                                </div>

                            @endif

                        </div>

                    </div>

                    <div id="transactions-panel" class="panel panel-primary" ng-class="visible">
                        <div class="panel-heading">Transacciones</div>

                        <div class="panel-body">

                            @if (count($transactions))

                                <div class="transactions-info" ng-cloak>
                                    <span class="label label-success">Ingresos:</span> [[ getTotal(filteredData)[0] | currency:undefined:0 ]]
                                    <span class="label label-danger">Gastos:</span> [[ getTotal(filteredData)[1] | currency:undefined:0 ]]
                                    <span class="label label-danger pull-right">Pagos urgentes: [[ needsPayment ]]</span>
                                </div>

                            @endif

                            @if (!count($transactions))

                                <div>No tienes transacciones aún.</div>

                            @else

                                <div class="table-responsive" ng-style="{height: tableHeight + 'px'}">
                                <table class="table table-striped table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <th>
                                                <a href="javascript:;" ng-click="sortType = 'title'; sortReverse = !sortReverse">
                                                    Nombre
                                                    <span ng-show="sortType == 'title' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'title' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>
                                                </a>                        
                                            </th>
                                            <th class="hidden-xs">
                                                <a href="javascript:;" ng-click="sortType = 'type_id'; sortReverse = !sortReverse">
                                                    Tipo
                                                    <span ng-show="sortType == 'type_id' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'type_id' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>                          
                                                </a>                              
                                            </th>
                                            <th>
                                                <a href="javascript:;" ng-click="sortType = 'category_id'; sortReverse = !sortReverse">
                                                    Categoria
                                                    <span ng-show="sortType == 'category_id' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'category_id' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>
                                                </a>                          
                                            </th>
                                            <th>
                                                <a href="javascript:;" ng-click="sortType = 'amount_paid'; sortReverse = !sortReverse">
                                                    Monto
                                                    <span ng-show="sortType == 'amount_paid' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'amount_paid' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>
                                                </a>                          
                                            </th>
                                            <th>
                                                <a href="javascript:;" ng-click="sortType = 'account_id'; sortReverse = !sortReverse">
                                                    Tipo de pago
                                                    <span ng-show="sortType == 'account_id' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'account_id' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>
                                                </a>                          
                                            </th>                        
                                            <th>
                                                <a href="javascript:;" ng-click="sortType = 'paid_date'; sortReverse = !sortReverse">
                                                    Fecha de Pago
                                                    <span ng-show="sortType == 'paid_date' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'paid_date' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>
                                                </a>                          
                                            </th>
                                            <th>
                                                <a href="javascript:;" ng-click="sortType = 'payment_date_limit'; sortReverse = !sortReverse">
                                                    Fecha límite de pago
                                                    <span ng-show="sortType == 'payment_date_limit' && !sortReverse" class="glyphicon glyphicon-triangle-bottom"></span>
                                                    <span ng-show="sortType == 'payment_date_limit' && sortReverse" class="glyphicon glyphicon-triangle-top"></span>
                                                </a>                          
                                            </th>
                                            <th class="hidden">Fecha de publicación</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr ng-repeat="transaction in ( filteredData = ( transactions | filter: filterData | orderBy:['needs_payment', sortType]:sortReverse ) )" ng-class="getColorClass(transaction)" ng-click="open( 'update', transaction )" ng-cloak>

                                            <td>[[ transaction.title ]]</td>
                                            <td class="hidden-xs">[[ transaction.type ]]</td>
                                            <td>[[ transaction.category ]]</td>
                                            <td>[[ transaction.amount_paid | currency:undefined:0 ]]</td>
                                            <td>[[ transaction.payment_type ]]</td>
                                            <td>[[ transaction.paid_date | mydatefilter:'MMMM dd yyyy' ]]</td>
                                            <td>[[ transaction.payment_date_limit | mydatefilter:'MMMM dd yyyy' ]]</td>
                                            <td class="hidden">[[ transaction.published_date ]]</td>
                                        </tr>

                                    </tbody>
                                </table>
                            
                            @endif

                            </div>

                        </div>
                    </div>
                </div>

            </div>

            @if (count($transactions))

                <div id="graphics-panel" class="col-lg-5" ng-class="visible">
                    <div id="polar-category" class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Gastos por categoría</div>
                            <div class="panel-body">
                                <div class="chart-container">
                                    <canvas id="polar-area" class="chart chart-polar-area" chart-data="data" chart-labels="labels"></canvas>
                                </div>
                            </div>
                        </div>            
                    </div>
                    <div id="bar-payment-type" class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Ingresos vs Gastos (Débito/Crédito)</div>
                            <div class="panel-body">
                                <div class="chart-container">
                                    <canvas class="chart chart-base" chart-type="type" chart-data="data2" chart-labels="labels2" chart-colours="colours2"></canvas>
                                </div>
                            </div>
                        </div>            
                    </div>                
                </div>  

            @endif
                          
        </div>

        @section('javascript')
            <script type="text/javascript">
              var expense_index = '{{ config('constants.TYPE_INDEX.expense') }}';
              var income_index = '{{ config('constants.TYPE_INDEX.income') }}';
              var debit_index = '{{ config('constants.PAYMENT_TYPE_INDEX.debit') }}';
              var credit_index = '{{ config('constants.PAYMENT_TYPE_INDEX.credit') }}';              
              var income_type = '{{ config('constants.TYPE.income') }}';
              var expense_type = '{{ config('constants.TYPE.expense') }}';
              var transactions_create_route = '{{ route('transactions.create') }}';
              var transactions_store_route = '{{ route('transactions.store') }}';
              var calendar_initial_day = {{ $calendar_initial_day }};
            </script>

            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.0/angular.js"></script>
            <script type="text/javascript" src="/js/Chart.min.js"></script>            
            <script type="text/javascript" src="/js/Chart.StackedBar.js"></script>            
            <script type="text/javascript" src="/js/angular-chart.js"></script>            
            <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.js"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-i18n/1.5.0/angular-locale_es-co.min.js"></script>
            <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-messages.js"></script>  
            <script type="text/javascript" src="/js/ui-bootstrap-tpls-1.1.2.min.js"></script>    
            <script type="text/javascript" src="/js/chart-functions.js"></script>
            <script type="text/javascript" src="/js/home.js"></script>
        @stop 

    @else

        <div id="welcome" class="row">
            <div class="col-md-6">
                <div class="jumbotron">
                    <h2>Lleva tus cuentas de manera sencilla.</h2>
                    <p>Mis Cuentas Online es un sencillo sistema de contabilidad personal totalmente gratuito.</p>
                    <ul>
                        <li>Registra tus gastos e ingresos.</li>
                        <li>Crea categorias.</li>
                        <li>Crea gastos e ingresos fijos mensuales.</li>
                        <li>Cuenta con varios gráficos que te permiten visualizar en qué categorias gastas tu dinero.</li>
                    </ol>
                </div>
            </div>
            <div class="col-md-6">
                @include('auth._register')
            </div>
        </div>

    @endif

@stop