(function(){
    var app = angular.module('miscuentas', ['ngAnimate', 'ui.bootstrap', 'ngMessages', 'chart.js']);

    //change angular tags
    app.config(function($interpolateProvider, ChartJsProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');

        // Configure all charts
        ChartJsProvider.setOptions({
            colours: ['#DC3912', '#FF9900', '#198C19', '#4682B4', '#00833E', '#2516FE', '#34495E'],
            responsive: true,
            // animation: false,
            animationSteps : 40,
            scaleShowLabels: true,
            scaleFontSize: 13,
            showTooltips: true,
            barValueSpacing : 75,
            // barDatasetSpacing : 10,
            // tooltipEvents: [],
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            onAnimationComplete: function()
            {
                // this.showTooltip(this.segments, true);

                //Show tooltips in bar chart (issue: multiple datasets doesnt work http://jsfiddle.net/5gyfykka/14/)
                //this.showTooltip(this.datasets[0].bars, true);

                //Show tooltips in line chart (issue: multiple datasets doesnt work http://jsfiddle.net/5gyfykka/14/)
                //this.showTooltip(this.datasets[0].points, true);  
            },
        });
        // Configure all doughnut charts
        ChartJsProvider.setOptions('Doughnut', {
          animateScale: true
        });      
    });

    app.factory('Data', function () {
        return { transactions: []};
    });

    app.controller('HomeController', ['$scope', '$http', '$uibModal', function($scope, $http, $uibModal){

        //loading vars
        $scope.visible = "not-visible";
        $scope.loadingText = "";
        $scope.needsPayment = 0;

        $scope.tableHeight = window.innerHeight - 320;

        $scope.transactions = [];

        //filter
        $scope.filterData   = '';     // set the default search/filter term
        $scope.filteredData  = [];     // set the default search/filter term

        //table sort
        $scope.sortType     = 'published_date'; //  set the default sort type
        $scope.sortReverse  = true;  // set the default sort order             

        //get current date
        var date = new Date();

        var today = date.getDate();

        if ( today > calendar_initial_day ){    
            $scope.dateStart = new Date(date.getFullYear(), date.getMonth(), calendar_initial_day);
            $scope.dateEnd = new Date(date.getFullYear(), date.getMonth() + 1, calendar_initial_day);
        }
        else{
            $scope.dateStart = new Date(date.getFullYear(), date.getMonth() - 1, calendar_initial_day);
            $scope.dateEnd = new Date(date.getFullYear(), date.getMonth(), calendar_initial_day);            
        }

        $scope.format = 'MMMM dd yyyy';

        $scope.openDateStart = false;                
        $scope.openDateEnd = false;                

        $scope.openDatePickerStart = function() {
            $scope.openDateStart = true;
        };

        $scope.openDatePickerEnd = function() {
            $scope.openDateEnd = true;
        };            

        //fetches data again when date is changed
        $scope.onDateChange = function(){
            //sets the last day of selected month
            // var dateEnd = new Date($scope.dateStart.getFullYear(), $scope.dateStart.getMonth() + 1, 0);

            getData($scope.dateStart, $scope.dateEnd);
        }

        //fetches data from server
        var getData = function(dateStart, dateEnd) {

            $scope.loadingText = "Cargando datos...";

            //sets the first day of given month in timestamp
            var dateStartTimestamp = dateStart.getTime() / 1000;
            //sets the last day of given month in timestamp
            var dateEndTimestamp = dateEnd.getTime() / 1000;

            $http.get( '/api/transactions?date-start=' + dateStartTimestamp + '&date-end=' + dateEndTimestamp )
            .success(function(data){
                $scope.transactions = data.transactions;

                $scope.loadingText = "Creando gráficos..."

                var paidExpensesTransactions = getPaidTransactions($scope.transactions, expense_index);
                $scope.labels = getCategories(paidExpensesTransactions); 
                $scope.data = getCategoriesValues($scope.labels, paidExpensesTransactions);

                var paidIncomeTransactions = getPaidTransactions($scope.transactions, income_index);
                var totalIncome = getTotalIncome(paidIncomeTransactions);
                var totalExpenses = getDebitCreditValues(paidExpensesTransactions);
                $scope.labels2 = ['Entradas', 'Gastos'];
                $scope.colours2 = ['#DC3912', '#2e57ff'];
                $scope.type = 'StackedBar';
                $scope.data2 = [
                  [0, totalExpenses[1]],
                  [totalIncome, totalExpenses[0]]
                ];

                $scope.loadingText = "";
                $scope.visible = "visible";
            })
            .error(function(data, status) {
                $scope.loadingText = "Hubo un error cargando los datos: " + status;
            });  
        }

        getData($scope.dateStart, $scope.dateEnd);

        // $scope.$watch('filterData', function() {
        //     var paidTransactions = getPaidTransactions($scope.filteredData, expense_index);
        //     $scope.labels = getCategories(paidTransactions); 
        //     $scope.data = getCategoriesValues($scope.labels, paidTransactions);
        // });

        //sum totals
        $scope.getTotal = function(transactions){
            var totalIncomes = 0;
            var totalExpenses = 0;

            transactions.forEach( function(transaction, index) {

                //if the transaction has been paid then add to total sum
                if (transaction.paid_date){
                    if ( transaction.type_id == expense_index ){
                        totalExpenses += parseInt(transaction.amount_paid);
                    }
                    else if ( transaction.type_id == income_index ){
                        totalIncomes += parseInt(transaction.amount_paid);
                    }  
                } 
            });

            return [totalIncomes, totalExpenses];
        }

        $scope.getColorClass = function(transaction){

            if ( transaction.needs_payment ) return 'extreme-danger';

            if ( !transaction.paid_date ) return 'hidden';

            switch (transaction.type_id) {
                case expense_index:
                    return 'danger'
                case income_index:
                    return 'success'                  
                default:
                    return ''
            }
        };

        var ObjectLength = function( object ) {
            var length = 0;
            for( var key in object ) {
                if( object.hasOwnProperty(key) ) {
                    ++length;
                }
            }
            return length;
        };

        //get urgent payments
        $http.get( '/api/transactions?needs-payment=true' )
        .success(function(data){
            $scope.needsPayment = ObjectLength(data.transactions);
        });

        //add expense/income modal
        $scope.open = function(action, transaction) {

            if ( typeof transaction == 'number' ){
                var transactionTemp = transaction;
                transaction = {};                    
                transaction.type_id = transactionTemp;
            } 

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: transactions_create_route,
                controller: 'ModalInstanceCtrl',
                resolve: {
                    action: function () {
                        return action;
                    },
                    transaction: function () {
                        return transaction;
                    }
                }
            });

            modalInstance.result.then(function (newTransaction) {
                //controller.transactions.push(newTransaction);
                getData($scope.dateStart, $scope.dateEnd);
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);   

    // Please note that $uibModalInstance represents a modal window (instance) dependency.
    // It is not the same as the $uibModal service used above.

    app.controller('ModalInstanceCtrl', function ($http, $scope, $uibModalInstance, $filter, action, transaction) {

        var controller = this;
        controller.action = action;

        //disable submit button
        $scope.disable = false;

        $scope.transactionFormData = transaction;

        //set title
        var actionText = ( action == 'store' ) ? 'Crear' : 'Editar';
        var transactionTypeText = ( $scope.transactionFormData.type_id == income_index ) ? income_type : expense_type;

        $scope.title = actionText + ' ' + transactionTypeText;

        //format dates
        $scope.format = 'MMMM dd yyyy'; 

        $scope.transactionFormData.paid_date_temp = $scope.transactionFormData.paid_date ? new Date($scope.transactionFormData.paid_date.replace(/(.+) (.+)/, "$1T$2Z")) : new Date();

        $scope.transactionFormData.payment_date_limit_temp = $scope.transactionFormData.payment_date_limit ? new Date($scope.transactionFormData.payment_date_limit.replace(/(.+) (.+)/, "$1T$2Z")) : null;

        //get categories
        $scope.categories = [];

        $http.get( "api/categories?type-id=" + $scope.transactionFormData.type_id + "&order-by=title" )
        .success(function(data) {
            $scope.categories = data;
            var newOption = { id: -1, title: "--Crear categoria nueva--" };
            $scope.categories.unshift(newOption);
        });      

        $scope.isExpense = function(){
            return $scope.transactionFormData.type_id == expense_index;
        }   

        $scope.submit = function() {
            $scope.errors = null;

            //disable submit button
            $scope.disable = true;

            //trick to format dates from "2016-02-25T19:04:49.201Z" to "2016-02-25" so the server parses them correctly
            $scope.transactionFormData.paid_date = $filter('date')($scope.transactionFormData.paid_date_temp, 'yyyy-MM-dd'); 
            $scope.transactionFormData.payment_date_limit = $filter('date')($scope.transactionFormData.payment_date_limit_temp, 'yyyy-MM-dd'); 

            switch (controller.action) {
                case 'store':
                    var request = $http.post( transactions_store_route, $scope.transactionFormData );
                    break;
                case 'update':
                    var request = $http.put( transactions_store_route + '/' + $scope.transactionFormData.id, $scope.transactionFormData );
                    break;
                default:
                    var request = $http.post( transactions_store_route, $scope.transactionFormData );
                    break;
            }

            request.success(function(data) {
                $uibModalInstance.close();
            });

            request.error(function (error, status){
                if (status === 422){
                    $scope.errors = error;
                    $scope.disable = false;
                }
            });
        };            

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.delete = function(transaction) {
            $scope.disable = true;
            $http.delete( transactions_store_route + '/' + transaction.id )
            .success(function(data) {
                $uibModalInstance.close();
            })
            .error(function (error, status){
                if (status === 422){
                    $scope.errors = error;
                    $scope.disable = false;
                }
            });
        }

        //add new category
        $scope.showNewCategoryForm = false;

        $scope.checkAddNewCategory = function() {
            if($scope.transactionFormData.category_id === -1) {
                $scope.showNewCategoryForm = true;
            } else {
                $scope.showNewCategoryForm = false;
            }
        }

        $scope.addCategoryToDropDown = function(value) {
            var newCategoryOption = { title: value, type_id: $scope.transactionFormData.type_id };
            $http.post( 'api/categories', newCategoryOption )
            .success(function(data){
                var newOption = { id: data.id, title: data.title };
                $scope.categories.push(newOption);
                $scope.transactionFormData.category_id = data.id;
                $scope.showNewCategoryForm = false;
            });
        }

        $scope.cancelNewCategory = function(){
            $scope.showNewCategoryForm = false;
            $scope.transactionFormData.category_id = null;
        }   

    });

    app.filter('mydatefilter', [
        '$filter', function($filter) {
            return function(input, format) {
                if (input){
                    //regexp to remove date incompatibility between browsers
                    input = input.replace(/(.+) (.+)/, "$1T$2Z");
                    return $filter('date')(new Date(input), format);
                }
                return '';
            };
        }
    ]);

    app.directive('ngConfirmClick', [
        function(){
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click',function (event) {
                        if ( window.confirm(msg) ) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
        }
    ]);

})();  