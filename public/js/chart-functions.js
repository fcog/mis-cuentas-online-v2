/**
* Returns if the transaction has been paid
* if paid_date of a transaction is null then this transaction has not been paid
*
* @param: object transaction
* @return boolean
*/
var isPaid = function(transaction){
    return transaction.paid_date !== null;
}
/**
* Returns the transaction that have been paid
* if paid_date of a transaction is null then this transaction has not been paid
*
* @param: array transactions 
* @param: int type_id 
* @return array
*/
var getPaidTransactions = function(transactions, type_id){

    var paidTransactions = [];

    switch (type_id) {
        case income_index:
            filter = function(transaction) { return transaction.type_id == income_index }
            break;
        case expense_index:
            filter = function(transaction) { return transaction.type_id == expense_index }
            break;
        default:
            filter = true;
            break;
    }

    transactions.forEach( function(transaction, index) {

        if( transaction.paid_date !== null && filter(transaction)){
            paidTransactions.push(transaction);
        }
    });   

    return paidTransactions;
}

/**
* Returns the unique categories in a given transaction array
*
* @param: array transactions 
* @return array
*/
var getCategories = function(transactions){
    var unique = {};
    var distinct = [];

    transactions.forEach( function(transaction, index) {

        if( typeof(unique[transaction.category]) == "undefined" ){
            if (transaction.paid_date !== null)
            distinct.push(transaction.category);
        }
        unique[transaction.category] = 0;
    });   

    return distinct;         
}

/**
* Returns the sum of the amount paid for each category in a transaction array
*
* @param: array categories 
* @param: array transactions 
* @return array
*/
var getCategoriesValues = function(categories, transactions){
    var values = [];

    categories.forEach( function(category, index1) {
        values[index1] = 0;

        transactions.forEach( function(transaction, index2) {
            if (transaction.category == category && isPaid(transaction)){
                values[index1] += parseInt(transaction.amount_paid);
            }
        });
    });  

    return values; 
}

/**
* Returns the sum of the amount paid in debit and credit
*
* @param: array transactions 
* @return array
*/
var getDebitCreditValues = function(transactions){
    var debit = 0;
    var credit = 0;

    transactions.forEach( function(transaction) {
        if (transaction.payment_type_id == debit_index){
            debit += parseInt(transaction.amount_paid);
        }
        else if (transaction.payment_type_id == credit_index){
            credit += parseInt(transaction.amount_paid);
        }
    });

    return [debit, credit]; 
}

/**
* Returns the sum of the total income
*
* @param: array transactions 
* @return int
*/
var getTotalIncome = function(transactions){
    var income = 0;

    transactions.forEach( function(transaction) {
        if (transaction.type_id == income_index){
            income += parseInt(transaction.amount_paid);
        }
    });

    return income; 
}