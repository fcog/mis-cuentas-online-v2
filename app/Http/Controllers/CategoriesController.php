<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class CategoriesController extends Controller
{
    private $types;

	/**
	 * Loads Authentication middleware for all methods
	 *
	 * CategoriesController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');

        $this->types = Collection::make(['Seleccione...', config('constants.TYPE.expense'), config('constants.TYPE.income')]);
	}

	/**
	 * View the active categories
	 *
	 * @return \Illuminate\View\View
     */
	public function index()
	{
		$categories = Auth::user()->categories;

		return view('categories.index', compact('categories'));
	}

	/**
	 * View the inactive categories
	 *
	 * @return \Illuminate\View\View
     */
	public function indexInactive()
	{
		$categories = Auth::user()->categories()->onlyTrashed()->get();

		return view('categories.index_inactive', compact('categories'));
	}

	/**
	 * View the create a category page
	 *
	 * @return \Illuminate\View\View
     */
	public function create()
	{
		$types = $this->types;

		return view('categories.create', compact('types'));
	}

	/**
	 * View the edit a category page
	 *
	 * @param Category $category
	 * @return \Illuminate\View\View
     */
	public function edit(Category $category)
	{
        $types = $this->types;

		return view('categories.edit', compact('category', 'types'));
	}

	/**
	 * Creates a category
	 *
	 * @param CategoryRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function store(CategoryRequest $request)
	{
		$stored = Auth::user()->categories()->create($request->all());

        if ($stored)
        {
            session()->flash('flash_message', 'La categoria ha sido creada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error creando la categoria.');
            session()->flash('flash_action', 'danger');
        }

		return redirect('categories');
	}


    /**
	 * Creates a category (version used in Transaction form)
	 *
	 * @param CategoryRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function store2(CategoryRequest $request)
	{
		$stored = Auth::user()->categories()->create($request->all());

        if ($stored)
        {
            return response()->json($stored, 200);
        }
        else
        {
            return response()->json(['responseText' => array('Hubo un error creando la categoria.')], 422); 
        }
	}

	/**
	 * Activates a category
	 *
	 * @param Category $category
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function softStore(Category $category)
	{
		$activated = $category->restore();

        if ($activated)
        {
            session()->flash('flash_message', 'La categoria ha sido activada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error activando la categoria.');
            session()->flash('flash_action', 'danger');
        }

		return redirect()->route('categories.indexInactive');
	}

	/**
	 * Updates a category
	 *
	 * If the category is trashed then the category is activated
	 *
	 * @param Category $category
	 * @param CategoryRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function update(Category $category, CategoryRequest $request)
	{
		$updated = $category->update($request->all());

        if ($updated)
        {
            session()->flash('flash_message', 'La categoria ha sido actualizada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error actualizando la categoria.');
            session()->flash('flash_action', 'danger');
        }

		return redirect('categories');
	}

	/**
	 * Inactivates a category
	 *
	 * @param Category $category
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
     */
	public function softDelete(Category $category)
	{
		$deactivated = $category->delete();

        if ($deactivated)
        {
            session()->flash('flash_message', 'La categoria ha sido desactivada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error desactivando la categoria.');
            session()->flash('flash_action', 'danger');
        }

		return redirect('categories');
	}

	/**
	 * Deletes a category
	 *
	 * @param Category $category
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function destroy(Category $category)
	{
        $deleted = $category->forceDelete();

        if (is_null($deleted))
        {
            session()->flash('flash_message', 'La categoria ha sido borrada.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error borrando la categoria.');
            session()->flash('flash_action', 'danger');
        }

		return redirect('categories');
	}

    /**
     * Returns a user's category by multiple options in JSON format (API).
     *
     * @return mixed
     */
    public function getUserCategories()
    {
    	return Category::getUserCategories(\Request::all());
    }

    /**
     * Returns a user's category by type in JSON format.
     * Used for jQuery auto completion in create a Fixed Transaction update form
     *
     * @param $type_id
     * @return mixed
     */
    public function getUserCategoriesByType($type_id)
    {
        return Category::getUserCategoriesByType($type_id);
    }    
}
