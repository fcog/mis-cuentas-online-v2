<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use Jenssegers\Date\Date;
use App\Http\Requests\TransactionRequest;
use App\Http\Requests;
use App\FixedTransaction;
use App\Transaction;
use App\Category;
use App\AccountStatistic;
use App\GeneralStatistic;


class TransactionsController extends Controller
{

    /**
     * Loads Authentication middleware for all methods
     *
     * FixedTransactionController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Displays the create an expense page
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Creates a new expense
     *
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(TransactionRequest $request)
    {
//        $stored = Auth::user()->transactions()->create($request->all());
        $transaction              = new Transaction();
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->user_id     = Auth::user()->id;
        $transaction->category_id = $request->input('category_id');
        $transaction->payment_type_id  = $request->input('payment_type_id');
        $transaction->type_id     = $request->input('type_id');
        $transaction->paid_date   = $request->input('paid_date');
        $transaction->payment_date_limit   = $request->input('payment_date_limit');
        // This conditional is for displaying the transactions in the correct month of payment. If it has been paid it is displayed with the creation date
        $transaction->published_date = empty( $request->input('paid_date') ) ? Date::now() : $request->input('paid_date');

        DB::beginTransaction();

        $stored = $transaction->save();

        if ($stored)
        {
            DB::commit();

            return response()->json(['responseText' => 'Success!'], 200);
        }
        else
        {
            DB::rollBack();

            return response()->json(['responseText' => array('Hubo un error creando el gasto.')], 422); 
        }
    }


    /**
     * Updates an expense
     *
     * @param Transaction $transaction
     * @param TransactionRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Transaction $transaction, TransactionRequest $request)
    {
//        $updated = $transaction->update($request->all());
        $transaction->title       = $request->input('title');
        $transaction->amount_paid = $request->input('amount_paid');
        $transaction->category_id = $request->input('category_id');
        $transaction->payment_type_id  = $request->input('payment_type_id');
        $transaction->paid_date   = $request->input('paid_date');
        $transaction->payment_date_limit   = $request->input('payment_date_limit');

        // This conditional is for displaying the transactions in the correct month of payment. If it has not been paid it is displayed with the creation date
        $transaction->published_date = empty($request->input('paid_date')) ? $transaction->created_at : $request->input('paid_date');
        
        DB::beginTransaction();

        $updated = $transaction->update();        

        if ($updated)
        {
            DB::commit();

            return response()->json(['responseText' => 'Success!'], 200);
        }
        else
        {
            DB::rollBack();

            return response()->json(['responseText' => array('Hubo un error creando el gasto.')], 422); 
        }
    }

    /**
     * Deletes an expense
     *
     * @param Transaction $transaction
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Transaction $transaction)
    {
        DB::beginTransaction();

        $deleted = $transaction->delete();

        if ($deleted)
        {
            DB::commit();
            
            return response()->json(['responseText' => 'Success!'], 200);
        }
        else
        {
            DB::rollBack();

            return response()->json(['responseText' => array('Hubo un error creando el gasto.')], 422); 
        }
    }

    public function getTransactions()
    {
        return Transaction::getUserTransactions(\Request::all());
    } 
}
