<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class UsersController extends Controller
{
	/**
	 * Loads Authentication middleware for all methods
	 *
	 * UserController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * View the user profile page
	 *
	 * @return \Illuminate\View\View
     */
	public function index()
	{
		$user = Auth::user();

		return view('users.edit', compact('user'));
	}	

	/**
	 * Updates a user
	 *
	 * @param User $user
	 * @param UserRequest $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function update(User $user, UserRequest $request)
	{
		$updated = $user->update($request->all());

        if ($updated)
        {
            session()->flash('flash_message', 'Datos actualizados correctamente.');
            session()->flash('flash_action', 'success');
        }
        else
        {
            session()->flash('flash_message', 'Hubo un error actualizando los datos.');
            session()->flash('flash_action', 'danger');
        }

		return redirect('users');
	}	

	/**
	 * Logout user and redirect to recover password
	 *
	 * @return \Illuminate\View\View
     */
	public function changePassword()
	{
		Auth::logout();
        return redirect('/password/email');
	}	

}

?>