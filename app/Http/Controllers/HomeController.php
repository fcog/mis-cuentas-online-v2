<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\AccountStatistic;
use App\Transaction;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        if (Auth::user())
        {
            if (!is_null($request->get('search')) && !empty(trim($request->get('search'))))
            {
                $search = $request->get('search');
                // Get the transactions that have been paid grouped by month and year
                $transactions = Auth::user()->transactions()->order()->search($search)->get();

                $transactions = Transaction::groupByMonth($transactions);

                return view('search', compact('user', 'transactions'));
            }
            else
            {
                // Used to check if user has categories
                $categories = Auth::user()->categories()->limit(1)->get();

                // Used to check if user has transactions
                $transactions = Auth::user()->transactions()->limit(1)->get();

                $calendar_initial_day = Auth::user()->calendar_initial_day;

                return view('home', compact('user', 'categories', 'transactions', 'calendar_initial_day'));
            }
        }
        else
        {
            return view('home', compact('user'));
        }
    }

}
