<?php

namespace App\Http\Requests;


class TransactionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $paid_date_validation = 'date_format:Y-m-d';
        $payment_type_validation = '';

        if( !empty( Request::get('paid_date') ) && Request::get('type_id') == config('constants.TYPE_INDEX.expense') )
        {
            $payment_type_validation = 'required|digits_between:1,2';
        } 

        if( !empty( Request::get('payment_type_id') ) )
        {
            $paid_date_validation .= '|required';
        }     

        return [
            'title' => 'required|min:3',
            'amount_paid' => 'required|regex:/^[\d*\.?\d*]+$/',
            'category_id' => 'exists:categories,id',
            'paid_date' => $paid_date_validation,
            'payment_type_id' => $payment_type_validation
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El título es obligatorio',
            'payment_type_id.digits_between' =>'El tipo de pago es obligatorio cuando se ingresa una fecha de pago',
            'payment_type_id.required' =>'El tipo de pago es obligatorio cuando se ingresa una fecha de pago',
            'paid_date.required' =>'La fecha de pago es obligatoria cuando se selecciona una cuenta',
            'title.min' => 'El título está muy corto',
            'amount_paid.required' => 'El monto es obligatorio',
            'amount_paid.regex' => 'El monto debe ser un valor numérico',
            'category_id.exists' => 'La categoría es obligatoria',
            'paid_date.date_format' => 'La fecha de pago debe tener el formato mes dia año'
        ];
    }
}
