<?php

namespace App\Http\Requests;


class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|max:255',
            'calendar_initial_day' => 'between:1,31'
        ];
    }

    /**
     * Custom validation error messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'El nombre es obligatorio',
            'name.min' => 'El nombre está muy corto',
            'calendar_initial_day.between' => 'El dia inicial del calendario debe ser un número entre 1 y 31',
        ];
    }
}
