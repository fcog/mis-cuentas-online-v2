<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
| Route Model Binding defined in RouteServiceProvider Class
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


/**
 *
 * Categories Routes
**/
// Get the current user's category by type in json format, used in fixed transaction form
Route::get('api/categories/type/{type_id}', 'CategoriesController@getUserCategoriesByType');

// view inactive/trashed categories
Route::get('categories/inactive', ['as' => 'categories.indexInactive', 'uses' => 'CategoriesController@indexInactive']);

// deactivate/trash categories
Route::get('categories/{categories}/deactivate', ['as' => 'categories.deactivate', 'uses' => 'CategoriesController@softDelete']);

// activate trashed categories
Route::get('categories/{categories}/activate', ['as' => 'categories.activate', 'uses' => 'CategoriesController@softStore']);

// creates RESTFUL routes
Route::resource('categories', 'CategoriesController');


/**
 *
 * Fixed transactions Routes
 **/
// Create the monthly fixed transactions into transactions (for use with cron)
Route::get('fixed-transactions/sync-cron', 'FixedTransactionsController@generateMonthlyFixedTransactionsCron');

// Create the monthly fixed transactions into transactions 
Route::get('fixed-transactions/sync', ['as' => 'fixed-transactions.sync', 'uses' => 'FixedTransactionsController@generateMonthlyFixedTransactions']);

// view inactive/trashed fixed transactions
Route::get('fixed-transactions/inactive', ['as' => 'fixed-transactions.indexInactive', 'uses' => 'FixedTransactionsController@indexInactive']);

// deactivate/trash fixed transactions
Route::get('fixed-transactions/{fixed_transactions}/deactivate', ['as' => 'fixed-transactions.deactivate', 'uses' => 'FixedTransactionsController@softDelete']);

// activate trashed fixed transactions
Route::get('fixed-transactions/{fixed_transactions}/activate', ['as' => 'fixed-transactions.activate', 'uses' => 'FixedTransactionsController@softStore']);

// creates RESTFUL routes
Route::resource('fixed-transactions', 'FixedTransactionsController');


/**
 *
 * Transactions Routes
 **/
// creates RESTFUL routes
Route::resource('transactions', 'TransactionsController');


/**
 *
 * User Routes
 **/
// creates RESTFUL routes
Route::resource('users', 'UsersController');
Route::get('user/change-password', ['as' => 'users.changePassword', 'uses' => 'UsersController@changePassword']);

/**
 *
 * API Routes
**/
Route::get('api/categories', 'CategoriesController@getUserCategories');
Route::post('api/categories', 'CategoriesController@store2');

// Get the user's transactions in json format
Route::get('api/transactions', 'TransactionsController@getTransactions');