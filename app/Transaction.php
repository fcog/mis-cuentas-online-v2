<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class Transaction extends Model
{
    use PresentableTrait;

    protected $dates = ['paid_date', 'payment_date_limit', 'published_date'];

    protected $presenter = 'App\Presenters\TransactionPresenter';

    protected $fillable = [
        'title',
    	'fixed_transactions_id',
    	'amount_paid',
        'user_id',
    	'category_id',
        'payment_type_id',
        'type_id',
    	'paid_date',
    	'payment_date_limit',
    ];

    protected $appends = ['type', 'category', 'payment_type', 'needs_payment'];
    
    /**
     * Mutator (this is runned before saving the property in the database) for saving the Published Date with Hour and Minutes
     *
     * @param $date
     */
    public function setPublishedDateAttribute($date)
    {
        if (!empty($date) && !is_object($date))
        {
            $this->attributes['published_date'] = Carbon::createFromFormat('Y-m-d', $date);
        }
    }
    
    /**
     * Mutator (this is runned before saving the property in the database) for saving the Paid Date with Hour and Minutes
     *
     * @param $date
     */
    public function setPaidDateAttribute($date)
    {
        if (!empty($date) && !is_object($date))
        {
            $this->attributes['paid_date'] = Carbon::createFromFormat('Y-m-d', $date);
        }
        else if (empty($date))
        {
            $this->attributes['paid_date'] = null;
        }
    }

    /**
     * Mutator for saving the Payment Date Limit with Hour and Minutes
     *
     * @param $date
     */
    public function setPaymentDateLimitAttribute($date)
    {
        if (!empty($date))
        {
            $this->attributes['payment_date_limit'] = Carbon::createFromFormat('Y-m-d', $date);
        }
    }

    /**
     * Mutator for cleaning the amount to only numbers, because the form sends the field with the point character
     *
     * @param $amount_paid
     */
    public function setAmountPaidAttribute($amount_paid)
    {
        if (!empty($amount_paid))
        {
            $this->attributes['amount_paid'] = preg_replace('/[^0-9]+/', '', $amount_paid);
        }
    }

    /**
     * Returns the type name of the category
     *
     * @param  int  $category_id
     * @return string
     */
    public function getTypeAttribute()
    {
        switch ($this->type_id)
        {
            case config('constants.TYPE_INDEX.expense'):
                $type = config('constants.TYPE.expense');
                break;
            case config('constants.TYPE_INDEX.income'):
                $type = config('constants.TYPE.income');
                break;
            default:
                $type = null;
                break;
        }

        return $type;
    }   

    /**
     * Returns the type name of the category
     *
     * @param  int  $category_id
     * @return string
     */
    public function getCategoryAttribute()
    {
        return Category::withTrashed()->find($this->category_id)->title;
    }     

    /**
     * Returns the type name of the account
     *
     * @return string
     */
    public function getPaymentTypeAttribute()
    {
        switch ($this->payment_type_id)
        {
            case config('constants.PAYMENT_TYPE_INDEX.debit'):
                $type = config('constants.PAYMENT_TYPE.debit');
                break;
            case config('constants.PAYMENT_TYPE_INDEX.credit'):
                $type = config('constants.PAYMENT_TYPE.credit');
                break;
            default:
                $type = null;
                break;
        }

        return $type;
    }      

    /**
     * Returns if the transaction needs to be paid very soon
     *
     * @return string
     */
    public function getNeedsPaymentAttribute()
    {
        $needs_payment = false;

        //if the transaction hasn't been paid and the transaction has a payment date limit
        if ( is_null($this->paid_date) && $this->payment_date_limit )
        {
            //if the payment limit is 3 days from now
            if ( $this->payment_date_limit->diff( Date::now() )->days < 3 || $this->payment_date_limit < Date::now() )
            {
                $needs_payment = true;
            }
        }

        return $needs_payment;
    }         

    /**
     * Accessor Returns the paid amount in number format
     *
     * @param  int  $value
     * @return int
     */
    public function getAmountPaidAttribute($value)
    {
        return (int) $value;
    }     

    /**
     * Returns the type name of the transaction defined in constants file
     *
     * @return string
     */
    public function getType()
    {
        switch ($this->type_id)
        {
            case config('constants.TYPE_INDEX.expense'):
                $type = config('constants.TYPE.expense');
                break;
            case config('constants.TYPE_INDEX.income'):
                $type = config('constants.TYPE.income');
                break;
            case config('constants.TYPE_INDEX.transfer'):
                $type = config('constants.TYPE.transfer');
                break;
            default:
                $type = null;
                break;
        }

        return $type;
    }

    /**
     * Scope function.
     *
     * Get the expense transactions
     *
     * @param $query
     */
    public function scopeExpenses($query)
    {
     	$query->where('type_id', config('constants.TYPE_INDEX.expense'));
    }

    /**
     * Scope function.
     *
     * Get the paid expense transactions
     *
     * @param $query
     */
    public function scopePaidExpenses($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.expense'))->whereNotNull('account_id');
    }

    /**
     * Scope function.
     *
     * Get the incomes transactions
     *
     * @param $query
     */
    public function scopeIncomes($query)
    {
        $query->where('type_id', config('constants.TYPE_INDEX.income'));
    }


    public function scopeSearch($query, $search)
    {
        if (!empty($search))
        {
            $query->where('title', 'LIKE', '%' . $search . '%');
        }
    }

    /**
     * returns a collection of transactions grouped by month, year
     *
     * @param $query
     */
    public static function scopeOrder($query)
    {
        $query->orderBy('published_date', 'DESC')->orderBy('payment_date_limit');
    }

    /**
     * Get the user associated with the Transaction
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the category record associated with the Transaction
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * returns a collection of transactions grouped by month, year
     *
     * @param $transactions
     * @return
     * @internal param $query
     */
    public static function groupByMonth($transactions)
    {
        return $transactions->groupBy(
            function($item) {
                $published_date = Date::createFromFormat('Y-m-d H:i:s', $item->published_date);
                return $published_date->format('F, Y');
            });
    }

    /**
     * Get the logged user's transactions by multiple options
     *
     * @param $type_id
     * @return mixed
     */
    public static function getUserTransactions($params){

        $query = Transaction::where('user_id', Auth::user()->id);
        //dd($query->get());

        $date_start = null;
        $date_end = null;

        $flag_needs_payment = false;

        foreach ($params as $key => $value) 
        {
            switch ($key) 
            {
                case 'date-start':
                    $date = strtotime('-1 day', $value);
                    $date_start = date("Y-m-d\TH:i:s", $date);
                    $query = $query->where('published_date', '>=', $date_start);
                    break ;
                case 'date-end':
                    $date = strtotime('+1 day', $value);
                    $date_end = date("Y-m-d\TH:i:s", $date);
                    $query = $query->where('published_date', '<=', $date_end);
                    break ;
                case 'needs-payment':
                    $flag_needs_payment = true;
                    $query = $query->whereNotNull('payment_date_limit')->whereNull('paid_date');
            }
        }

        if ($flag_needs_payment){
            $transactions = $query->get()->filter(function($item) {
                return $item->needs_payment === true;
            });
        }
        else{
            $transactions = $query->get();
        }

        // $query = $query->orderBy('published_date', 'asc');

        return array( 'date-start' => $date_start, 'date-end' => $date_end, 'transactions' => $transactions );    
    }
}