<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Category;
use App\FixedTransaction;
use App\Account;
use App\User;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

//        $router->model('categories', 'App\Category');
//        $router->model('fixed-transactions', 'App\FixedTransaction');

        $router->model('transactions', 'App\Transaction');

        // all categories included trashed objects
        $router->bind('categories', function($id) {
            return Category::withTrashed()->find($id);
        });

        // all fixed expenses included trashed objects
        $router->bind('fixed_transactions', function($id) {
            return FixedTransaction::withTrashed()->find($id);
        });

        // all fixed expenses included trashed objects
        $router->bind('users', function($id) {
            return User::find($id);
        });        
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
